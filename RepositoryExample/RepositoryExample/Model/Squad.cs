﻿using System.Collections.Generic;
using RepositoryExample.Model.Interface;

namespace RepositoryExample.Model
{
    public class Squad : IDomainObject
    {
        private int _id;
        private bool _deleteflag;
        private IEnumerable<Unit> _units;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool Deleteflag
        {
            get { return _deleteflag; }
            set { _deleteflag = value; }
        }

        public IEnumerable<Unit> Units
        {
            get { return _units; }
            set { _units = value; }
        }
    }
}