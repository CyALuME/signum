﻿using System;
using System.Collections.Generic;
using System.Text;
using RepositoryExample.Model.Interface;

namespace RepositoryExample.Model
{
    public class GearItem : IDomainObject
    {
        private int _id;
        private bool _deleteflag;
        private int _strength;
        private int _armourPenetration;
        private int _attacks;
        private int _weaponType;
        private int _points;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool Deleteflag
        {
            get { return _deleteflag; }
            set { _deleteflag = value; }
        }

        public int Strength
        {
            get { return _strength; }
            set { _strength = value; }
        }

        public int ArmourPenetration
        {
            get { return _armourPenetration; }
            set { _armourPenetration = value; }
        }

        public int Attacks
        {
            get { return _attacks; }
            set { _attacks = value; }
        }

        public int WeaponType
        {
            get { return _weaponType; }
            set { _weaponType = value; }
        }

        public int points
        {
            get { return _points; }
            set { _points = value; }
        }
    }
}
