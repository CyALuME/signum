﻿using System;
using System.Collections.Generic;
using System.Text;
using RepositoryExample.Model.Interface;

namespace RepositoryExample.Model
{
    public class Equipment : IDomainObject
    {
        private int _id;
        private bool _deleteflag;
        private IEnumerable<GearItem> _gearItems;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool Deleteflag
        {
            get { return _deleteflag; }
            set { _deleteflag = value; }
        }

        public IEnumerable<GearItem> GearItems
        {
            get { return _gearItems; }
            set { _gearItems = value; }
        }
    }
}
