﻿using System;
using System.Collections.Generic;
using System.Text;
using RepositoryExample.Model.Interface;

namespace RepositoryExample.Model
{
    public class Unit : IDomainObject
    {
        #region private memebers

        private int _id;
        private string _name;
        private bool _leader;
        private bool _deleteflag;
        private int _movement;
        private int _wounds;
        private int _morale;
        private int _attacks;
        private int _strength;
        private int _bSkill;
        private int _wSkill;
        private int _unitType;
        private int _squadId;
        private int _points;
        private int _toughness;
        private int _leadership;
        private int _armour;
        private Equipment _equipment;

        #endregion

        #region Getter Setter

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool Deleteflag
        {
            get { return _deleteflag; }
            set { _deleteflag = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public bool Leader
        {
            get { return _leader; }
            set { _leader = value; }
        }

        public int Movement
        {
            get { return _movement; }
            set { _movement = value; }
        }

        public int Toughness
        {
            get { return _toughness; }
            set { _toughness = value; }
        }

        public int Wounds
        {
            get { return _wounds; }
            set { _wounds = value; }
        }

        public int Morale
        {
            get { return _morale; }
            set { _morale = value; }
        }

        public int Attacks
        {
            get { return _attacks; }
            set { _attacks = value; }
        }

        public int Leadership
        {
            get { return _leadership; }
            set { _leadership = value; }
        }

        public int Armour
        {
            get { return _armour; }
            set { _armour = value; }
        }

        public int Strength
        {
            get { return _strength; }
            set { _strength = value; }
        }

        public int BSkill
        {
            get { return _bSkill; }
            set { _bSkill = value; }
        }

        public int WSkill
        {
            get { return _wSkill; }
            set { _wSkill = value; }
        }

        public int UnitType
        {
            get { return _unitType; }
            set { _unitType = value; }
        }

        public int SquadId
        {
            get { return _squadId; }
            set { _squadId = value; }
        }

        public int Points
        {
            get { return _points; }
            set { _points = value; }
        }

        public Equipment Equipment
        {
            get { return _equipment; }
            set { _equipment = value; }
        }

        #endregion

            #region Contructor
        public Unit()
        {
        }

        #endregion
    }
}
