﻿namespace RepositoryExample.Model.Interface
{
    public interface IGearItem : IDomainObject
    {
        int Strength { get; set; }
        int ArmourPenetration { get; set; }
        int Attacks { get; set; }
        int WeaponType { get; set; }
        int points { get; set; }
    }
}