﻿using System.Runtime.InteropServices.ComTypes;
using RepositoryExample.Model.Interface;

namespace RepositoryExample.Model
{
    public interface IUnit : IDomainObject
    {
        string Name { get; set; }
        bool Leader { get; set; }
        int Movement { get; set; }
        int BSkill { get; set; }
        int WSkill { get; set; }
        int Strength { get; set; }
        int Toughness { get; set; }
        int Wounds { get; set; }
        int Attacks { get; set; }
        int Leadership { get; set; }
        int Armour { get; set; }
        int UnitType { get; set; }
        int SquadId { get; set; }
        int Points { get; set; }
    }
}