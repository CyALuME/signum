﻿using System.Collections.Generic;

namespace RepositoryExample.Model.Interface
{
    public interface ISquad :IDomainObject
    {
        IEnumerable<IUnit> Units { get; set; }
    }
}