﻿using System.Collections.Generic;

namespace RepositoryExample.Model.Interface
{
    public interface IEquipment : IDomainObject
    {
        IEnumerable<IGearItem> Loadout { get; set; }
    }
}