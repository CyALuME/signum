﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RepositoryExample.Model;
using RepositoryExample.Repository.Interface;
using RepositoryExample.Repositroy.Context;

namespace RepositoryExample.Repository
{
    public class SquadRepository : Repository<Squad>, ISquadRepository
    {
        public SquadRepository(DemoContext context)
            : base(context)
        {
        }

        public int GetPointsInSquad(int squadId)
        {
            var squadWithUnits = DemoContext
                .Squads
                .Where(c => c.Id == squadId)
                .Include(c => c.Units)
                .FirstOrDefault();

            return squadWithUnits?.Units.Sum(unit => unit.Points) ?? 0;
        }

        public IEnumerable<Unit> GetSquad(int squadId)
        {
            var squadWithUnits = DemoContext
                .Squads
                .Where(c => c.Id == squadId)
                .Include(c => c.Units)
                .FirstOrDefault();

            return squadWithUnits?.Units ?? null;
        }

        public Unit GetSquadLeaderForSquad(int squadId)
        {
            var squadWithUnits = DemoContext
                .Squads
                .Where(c => c.Id == squadId)
                .Include(c => c.Units)
                .FirstOrDefault();

            
        }
    }
}
