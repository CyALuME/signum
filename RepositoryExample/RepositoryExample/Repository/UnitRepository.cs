﻿using System.Collections.Generic;
using System.Linq;
using RepositoryExample.Model;
using RepositoryExample.Repository.Interface;
using RepositoryExample.Repositroy.Context;

namespace RepositoryExample.Repository
{
    public class UnitRepository : Repository<Unit>, IUnitRepository
    {
        public UnitRepository(DemoContext context) 
            : base(context)
        {

        }

        public IEnumerable<Unit> GetUnitsInSquad(int squadId)
        {
            return DemoContext
                .Set<Unit>()
                .Where(c => c.SquadId == squadId);
        }

        public int GetPointsforUnit(int id)
        {
            return DemoContext
                .Set<Unit>()
                .Find(id).Points;
        }
    }
}