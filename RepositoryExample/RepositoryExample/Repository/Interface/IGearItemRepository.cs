﻿using RepositoryExample.Model;

namespace RepositoryExample.Repository.Interface
{
    public interface IGearItemRepository : IRepository<GearItem>
    {
        int GetPointsforGearItem(int id);
    }
}