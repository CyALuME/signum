﻿using System.Collections.Generic;
using RepositoryExample.Model;

namespace RepositoryExample.Repository.Interface
{
    public interface ISquadRepository : IRepository<Squad>
    {
        int GetPointsInSquad(int squadId);
        Unit GetSquadLeaderForSquad(int squadId);
        IEnumerable<Unit> GetSquad(int squadId);
    }
}