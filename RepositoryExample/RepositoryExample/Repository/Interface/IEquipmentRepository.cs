﻿using System.Collections.Generic;
using RepositoryExample.Model;

namespace RepositoryExample.Repository.Interface
{
    public interface IEquipmentRepository : IRepository<Equipment>
    {
        IEnumerable<GearItem> GetGearforEquipment(int equipmentId);
    }
}