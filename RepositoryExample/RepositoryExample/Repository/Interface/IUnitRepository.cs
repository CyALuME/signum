﻿using System.Collections.Generic;
using RepositoryExample.Model;

namespace RepositoryExample.Repository.Interface
{
    public interface IUnitRepository : IRepository<Unit>
    {
        IEnumerable<Unit> GetUnitsInSquad(int squadId);
        int GetPointsforUnit(int id);
    }
}