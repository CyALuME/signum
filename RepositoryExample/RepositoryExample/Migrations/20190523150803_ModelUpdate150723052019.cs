﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RepositoryExample.Repository.Migrations
{
    public partial class ModelUpdate150723052019 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EquipmentId",
                table: "Units",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Units_EquipmentId",
                table: "Units",
                column: "EquipmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Units_Equipment_EquipmentId",
                table: "Units",
                column: "EquipmentId",
                principalTable: "Equipment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Units_Equipment_EquipmentId",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_Units_EquipmentId",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "EquipmentId",
                table: "Units");
        }
    }
}
