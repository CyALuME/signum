﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RepositoryExample.Repository.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleteflag = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Leader = table.Column<bool>(nullable: false),
                    Movement = table.Column<int>(nullable: false),
                    Toughness = table.Column<int>(nullable: false),
                    Wounds = table.Column<int>(nullable: false),
                    Morale = table.Column<int>(nullable: false),
                    Attacks = table.Column<int>(nullable: false),
                    Leadership = table.Column<int>(nullable: false),
                    Armour = table.Column<int>(nullable: false),
                    Strength = table.Column<int>(nullable: false),
                    BSkill = table.Column<int>(nullable: false),
                    WSkill = table.Column<int>(nullable: false),
                    UnitType = table.Column<int>(nullable: false),
                    SquadId = table.Column<int>(nullable: false),
                    Points = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Units");
        }
    }
}
