﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using RepositoryExample.Model;
using RepositoryExample.Model.Interface;

namespace RepositoryExample.Repositroy.Context
{
    public sealed class DemoContext : DbContext, IDemoContext
    {
        //public DemoContext(DbContextOptions<DemoContext> options) : base(options)
        //{
        //    if (ChangeTracker != null)
        //    {
        //        ChangeTracker.LazyLoadingEnabled = false;
        //    }
        //}

        public DemoContext()
            : base()
        {
            if (ChangeTracker != null)
            {
                ChangeTracker.LazyLoadingEnabled = true;
            }
        }

        public DbSet<Unit> Units { get; set; }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<GearItem> Gear { get; set; }
        public DbSet<Squad> Squads { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=localhost;Database=RepoExample;Trusted_Connection=True;");
            }
        }
    }
}