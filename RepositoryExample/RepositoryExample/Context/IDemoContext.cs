﻿using Microsoft.EntityFrameworkCore;
using RepositoryExample.Model;

namespace RepositoryExample.Repositroy.Context
{
    public interface IDemoContext
    {
        DbSet<Equipment> Equipment { get; set; }
        DbSet<GearItem> Gear { get; set; }
        DbSet<Squad> Squads { get; set; }
        DbSet<Unit> Units { get; set; }

    }
}