﻿using RepositoryExample.Repository.Interface;
using System;
using System.ComponentModel;
using Newtonsoft.Json.Bson;
using RepositoryExample.Repositroy.Context;

namespace RepsoitoryExample.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ISquadRepository Squads { get; }
        IUnitRepository Units { get; }

        int SaveChanges();
        void CommitTransaction();
        void BeginTransaction();
        void RollbackTransaction();
    }
     
}