﻿using System;
using RepositoryExample.Repository;
using RepositoryExample.Repository.Interface;
using RepositoryExample.Repositroy.Context;

namespace RepsoitoryExample.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DemoContext _context;

        public UnitOfWork(IDemoContext context)
        {
            //Populate our context
            _context = context as DemoContext;
            Squads = new SquadRepository(_context);
            Units = new UnitRepository(_context);
        }

        public ISquadRepository Squads { get; private set; }
        public IUnitRepository Units { get; private set; }

        public void CommitTransaction()
        {
            _context.Database.CommitTransaction();
        }

        public void RollbackTransaction()
        {
            _context.Database.BeginTransaction();
        }

        public void BeginTransaction()
        {
            _context.Database.RollbackTransaction();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
