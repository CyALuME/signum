﻿using System;
using System.ComponentModel;
using RepositoryExample.Repository.Interface;

namespace RepsoitoryExample.UnitOfwork
{
    public interface IUnitOfWork : IDisposable
    {
        IUnitRepository Units { get; }

        //ISquadRepository Squads { get; }

        //IGearItemRepository Gear { get; }

        //IEquipmentRepository Equipment { get; }

        int Complete();

    }
}