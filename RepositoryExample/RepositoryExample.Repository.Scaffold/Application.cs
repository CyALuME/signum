﻿using System;
using Autofac;
using Microsoft.EntityFrameworkCore;
using RepositoryExample.Model;
using RepositoryExample.Repositroy.Context;
using RepsoitoryExample.UnitOfWork;

namespace RepositoryExample.Repository.Scaffold
{
    public class Application : IApplication
    {
        private IUnitOfWork _unitOfWork;
        private IDemoContext _context;

        
        public Application(IUnitOfWork unitOfWork, IDemoContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public void Run()
        {
            var bootstrap = BootstrapConfig.GetContainer();

            using (var unitOfWork = bootstrap.Resolve<IUnitOfWork>())
            {
                try
                {
                   // unitOfWork.BeginTransaction();

                    //var squad = CreateSquad();

                    //var sm1 = CreateSM1();
                    //var sm2 = CreateSM2();
                    //var sm3 = CreateSM3();
                    //var sm4 = CreateSM4();
                    //var sm5 = CreateSM5();

                    //squad.Units = new Unit[] { sm1, sm2, sm3, sm4, sm5 };

                    //unitOfWork.Squads.Add(squad);
                    //unitOfWork.Units.AddRange(squad.Units);

                    //unitOfWork.SaveChanges();

                    var points = unitOfWork.Squads.GetPointsInSquad(1);

                    Console.WriteLine("Total points in squad: " + points.ToString());
                    Console.ReadLine();
                }
                catch ( Exception e)
                {
                    //unitOfWork.RollbackTransaction();
                }
                finally
                {
                    //unitOfWork.CommitTransaction();
                }
                
            }
            Console.WriteLine();
        }

        private static Squad CreateSquad()
        {
            var s = new Squad { Deleteflag = false };
            return s;
        }

        private static Unit CreateSM1()
        {
            var sm = new Unit
            {
                Leader = true,
                Armour = 3,
                Attacks = 3,
                BSkill = 3,
                Deleteflag = false,
                Name = "Marine 1",
                Wounds = 1,
                Morale = 8,
                Leadership = 8,
                Points = 14,
                Toughness = 4
            };
            return sm;
        }

        private static Unit CreateSM2()
        {
            var sm = new Unit
            {
                Leader = false,
                Armour = 3,
                Attacks = 2,
                BSkill = 3,
                Deleteflag = false,
                Name = "Marine 2",
                Wounds = 1,
                Morale = 7,
                Leadership = 7,
                Points = 14,
                Toughness = 4
            };
            return sm;
        }

        private static Unit CreateSM3()
        {
            var sm = new Unit
            {
                Leader = false,
                Armour = 3,
                Attacks = 2,
                BSkill = 3,
                Deleteflag = false,
                Name = "Marine 3",
                Wounds = 1,
                Morale = 7,
                Leadership = 7,
                Points = 14,
                Toughness = 4
            };
            return sm;
        }

        private static Unit CreateSM4()
        {
            var sm = new Unit
            {
                Leader = false,
                Armour = 3,
                Attacks = 2,
                BSkill = 3,
                Deleteflag = false,
                Name = "Marine 4",
                Wounds = 1,
                Morale = 7,
                Leadership = 7,
                Points = 14,
                Toughness = 4
            };


            return sm;
        }

        private static Unit CreateSM5()
        {
            var sm = new Unit
            {
                Leader = false,
                Armour = 3,
                Attacks = 2,
                BSkill = 3,
                Deleteflag = false,
                Name = "Marine 5",
                Wounds = 1,
                Morale = 7,
                Leadership = 7,
                Points = 14,
                Toughness = 4
            };


            return sm;
        }
    }
}