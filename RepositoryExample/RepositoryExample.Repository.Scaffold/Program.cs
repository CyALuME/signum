﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Core;
using Microsoft.EntityFrameworkCore;
using RepositoryExample.Model;
using RepositoryExample.Repository.Interface;
using RepositoryExample.Repositroy.Context;
using RepsoitoryExample.UnitOfWork;

namespace RepositoryExample.Repository.Scaffold
{
    class Program
    {
        static void Main(string[] args)
        {
            var bootstrap = BootstrapConfig.Configure();
            using (var scope = bootstrap.BeginLifetimeScope())
            {
                var app = scope.Resolve<IApplication>();
                app.Run();
            }
            Console.ReadLine();
        }
    }
}
