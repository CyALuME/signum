﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using RepositoryExample.Model;
using RepositoryExample.Repository.Interface;
using RepositoryExample.Repositroy.Context;
using RepsoitoryExample.UnitOfWork;

namespace RepositoryExample.Repository.Scaffold
{
    public static class BootstrapConfig
    {
        private static IContainer _container;
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<DemoContext>().As<IDemoContext>().WithParameter( new TypedParameter(typeof(DbContext), "_context"));
            builder.RegisterType<DbContext>().AsSelf();
            builder.RegisterType<DbContextOptions>().As<IDbContextOptions>();
            builder.RegisterType<Application>().As<IApplication>();


            // Repositorys 
            builder.RegisterType<UnitRepository>().As<IRepository<Unit>>();
            builder.RegisterType<EquipmentRepository>().As<IRepository<Equipment>>();
            builder.RegisterType<GearItemRepository>().As<IRepository<GearItem>>();
            builder.RegisterType<SquadRepository>().As<IRepository<Squad>>();

            _container = builder.Build();
            return _container;
        }

        public static IContainer GetContainer()
        {
            return _container;
        }
    }
}